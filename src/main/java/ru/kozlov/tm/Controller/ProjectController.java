package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public class ProjectController extends Controller {

    private TaskService taskService;
    private ProjectService projectService;
    private Scanner scanner = new Scanner(System.in);

    public ProjectController(
            ProjectService projectService,
            TaskService taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    public void create() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        System.out.println("Введите название проекта:");
        String name = scanner.nextLine().trim();

        System.out.println("Введите описание проекта:");
        String description = scanner.nextLine().trim();

        System.out.println("Введите дату начала проекта в формате \"30/12/1900\"");
        Date dateStart = dateFormat.parse(scanner.nextLine().trim());

        System.out.println("Введите дату завершения проекта в формате \"30/12/1900\"");
        Date  dateEnd = dateFormat.parse(scanner.nextLine().trim());

        Project project = projectService.createProject(name, description, dateStart, dateEnd);

        System.out.println("[ПРОЕКТ " + project.getName() + " СОЗДАН]");
    }

    public void index() {
        System.out.println("[СПИСОК ПРОЕКТОВ]");
        Map<String , Project> projects = projectService.getAllProjects();

        projects.forEach((integer, task) -> System.out.println(task));

        System.out.println("[OK]");
    }

    public void remove() {
        System.out.println("Введите id проекта:");
        String id = scanner.nextLine();

        try {
            projectService.removeProjectById(id);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        taskService.removeTasksByProjectId(id);

        System.out.println("[ПРОЕКТ #" + id + " УДАЛЕН]");
    }

    public void removeAll() {
        projectService.removeAllProjects();

        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }

    public void generate() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Введите кол-во проектов для генерации:");
        int count = Integer.parseInt(scanner.nextLine());

        for (int i = 1; i <= count; i++) {
            String name = "Проект #" + i;
            String description = "Описание проекта #" + i;
            Date dateStart = dateFormat.parse("30/12/1900");
            Date dateEnd = dateFormat.parse("30/12/1900");
            projectService.createProject(name, description, dateStart, dateEnd);
        }

        System.out.println("[ПРОЕКТЫ СГЕНЕРИРОВАНЫ]");
    }
}
