package ru.kozlov.tm.Controller;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public class TaskController extends Controller{

    private Scanner scanner = new Scanner(System.in);
    private TaskService taskService;
    private ProjectService projectService;

    public TaskController(
            TaskService taskService,
            ProjectService projectService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    public void create() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Map<String, Project> projects = projectService.getAllProjects();
        System.out.println("[ВАШИ ПРОЕКТЫ]");
        projects.forEach((string, project) -> System.out.println(project));

        System.out.println("Введите id проекта:");
        String projectId = scanner.nextLine().trim();

        System.out.println("Введите название задачи:");
        String taskName = scanner.nextLine().trim();

        System.out.println("Введите описание задачи:");
        String taskDescription = scanner.nextLine().trim();

        System.out.println("Введите дату начала задачи в формате \"30/12/1900\"");
        Date dateStart = dateFormat.parse(scanner.nextLine().trim());

        System.out.println("Введите дату завершения задачи в формате \"30/12/1900\"");
        Date  dateEnd = dateFormat.parse(scanner.nextLine().trim());

        Task task = taskService.createTask(projectId, taskName, taskDescription, dateStart, dateEnd);

        System.out.println("[ЗАДАЧА \"" + task.getName() + "\" СОЗДАНА]");
    }

    public void index() {
        System.out.println("Введите id проекта:");
        String  projectId = scanner.nextLine();
        try {
            Project project = projectService.getProjectById(projectId);

            Map<String, Task> tasks = taskService.getTasksByProjectId(project.getId());

            System.out.println("Кол-во задач:" + tasks.size());

            tasks.forEach((integer, task) -> System.out.println(task));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    public void remove() {
        System.out.println("Введите id задачи:");
        String id = scanner.nextLine().trim();

        try {
            taskService.removeTaskById(id);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());

            return;
        }

        System.out.println("[ЗАДАЧА #" + id + " УДАЛЕНА]");
    }

    public void removeAll() {
        System.out.println("Введите id проекта:");
        String projectId = scanner.nextLine().trim();

        try {
            projectService.getProjectById(projectId);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());

            return;
        }

        taskService.removeTasksByProjectId(projectId);


        System.out.println("[ЗАДАЧИ ПРОЕКТА #" + projectId + " УДАЛЕНЫ]");
    }

    public void generate() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Map<String, Project> projects = projectService.getAllProjects();

        for (Map.Entry<String, Project> projectEntry : projects.entrySet()) {
            int count = 1 + (int) (Math.random() * 8);

            String projectId = projectEntry.getKey();
            for (int i = 1; i <= count; i++) {
                String taskName = "Задача #" + i;
                String taskDescription = "Описание задачи #" + i;
                Date dateStart = dateFormat.parse("30/12/1900");
                Date dateEnd = dateFormat.parse("30/12/1900");
                taskService.createTask(projectId, taskName, taskDescription, dateStart, dateEnd);
            }
        }

        System.out.println("[ЗАДАЧИ СГЕНЕРИРОВАНЫ]");
    }

}
