package ru.kozlov.tm.Controller;

import java.text.ParseException;

public abstract class Controller {
    public abstract void create() throws ParseException;
    public abstract void index();
    public abstract void remove();
    public abstract void removeAll();
}
