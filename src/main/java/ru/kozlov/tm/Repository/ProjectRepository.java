package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Project;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ProjectRepository {
    private Map<String, Project> projects = new HashMap<>();

    public void removeAll() {
        projects.clear();
    }

    public Project create(
            String id,
            String name,
            String description,
            Date dateStart,
            Date dateEnd
    ) {
        Project project = new Project(id ,name, description, dateStart, dateEnd);

        projects.put(id , project);

        return project;
    }

    public Map<String, Project> getAll() {
        return projects;
    }

    public Project removeById(String id) {
        return projects.remove(id);
    }

    public Project getById(String id) {
        return projects.get(id);
    }
}
