package ru.kozlov.tm.Repository;

import ru.kozlov.tm.Entity.Task;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> tasks = new HashMap<>();


    public Task create(
            String taskId,
            String taskName,
            String taskDescription,
            Date dateStart,
            Date dateEnd,
            String projectId
    ) {
        Task task = new Task(taskId, taskName, taskDescription, dateStart, dateEnd, projectId);

        tasks.put(taskId, task);

        return task;
    }

    public Map<String, Task> gelAllByProjectId(String projectId) {
        Map<String, Task> projectTasks = new HashMap<>();

        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            if (task.getValue().getProjectId().equals(projectId)) {
                projectTasks.put(task.getKey(), task.getValue());
            }
        }

        return projectTasks;
    }

    public Task removeById(String id) {
        return tasks.remove(id);
    }

    public void removeAllByProjectId(String projectId) {
        Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, Task> taskEntry = it.next();

            if (taskEntry.getKey().equals(projectId)) {
                it.remove();
            }
        }
    }


    public Map<String, Task> getAll() {
        return tasks;
    }
}
