package ru.kozlov.tm;

import ru.kozlov.tm.Const.Command;
import ru.kozlov.tm.Controller.ProjectController;
import ru.kozlov.tm.Controller.TaskController;
import ru.kozlov.tm.Repository.ProjectRepository;
import ru.kozlov.tm.Repository.TaskRepository;
import ru.kozlov.tm.Service.ProjectService;
import ru.kozlov.tm.Service.TaskService;

import java.text.ParseException;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws ParseException {
        ProjectRepository projectRepository = new ProjectRepository();
        TaskRepository taskRepository = new TaskRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        TaskService taskService = new TaskService(taskRepository);
        ProjectController projectController = new ProjectController(projectService, taskService);
        TaskController taskController = new TaskController(taskService, projectService);

        Scanner scanner = new Scanner(System.in);
        String command;

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("Список команд:");
        help();

        do {
            System.out.println("Введите команду: ");
            command = scanner.nextLine().trim();

            switch (command) {
                case (Command.PROJECT_CLEAR): {
                    projectController.removeAll();

                    break;
                }
                case (Command.PROJECT_CREATE): {
                    projectController.create();

                    break;
                }
                case (Command.PROJECT_LIST): {
                    projectController.index();

                    break;
                }
                case (Command.PROJECT_REMOVE): {
                    projectController.remove();

                    break;
                }
                case (Command.TASK_CLEAR): {
                    taskController.removeAll();

                    break;
                }
                case (Command.TASK_CREATE): {
                    taskController.create();

                    break;
                }
                case (Command.TASK_LIST): {
                    taskController.index();

                    break;
                }
                case (Command.TASK_REMOVE): {
                    taskController.remove();

                    break;
                }
                case (Command.PROJECT_GENERATOR): {
                    projectController.generate();

                    break;
                }
                case (Command.TASK_GENERATOR): {
                    taskController.generate();

                    break;
                }
                case (Command.HELP): {
                    help();

                    break;
                }
                case (Command.EXIT): {
                    System.out.println("*** GOODBYE ***");

                    break;
                }
                default: {
                    System.out.println(
                            "Неизвестная команда \n" +
                            "Введите help чтобы просмотреть список команд"
                    );

                    break;
                }
            }
        } while (!command.equals("exit"));

    }

    public static void help() {
        System.out.println("help: Show all commands.");
        System.out.println("project-clear: Remove all projects.");
        System.out.println("project-create: Create new project.");
        System.out.println("project-list: Show all projects.");
        System.out.println("project-remove: Remove selected project.");
        System.out.println("task-clear: Remove all tasks.");
        System.out.println("task-create: Create new task.");
        System.out.println("task-list: Show all tasks.");
        System.out.println("task-remove: Remove selected tasks.");
        System.out.println("pg: Project generator.");
        System.out.println("tg: Task generator.");
    }

}
