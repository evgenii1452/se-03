package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Task;
import ru.kozlov.tm.Repository.TaskRepository;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task createTask(
            String projectId,
            String taskName,
            String taskDescription,
            Date dateStart,
            Date dateEnd
    ) {
        String taskId = UUID.randomUUID().toString();
        return taskRepository.create(taskId, taskName, taskDescription, dateStart, dateEnd, projectId);
    }

    public Map<String, Task> getTasksByProjectId(String projectId) {
        return taskRepository.gelAllByProjectId(projectId);
    }

    public void removeTaskById(String id) {
        Task removedTask = taskRepository.removeById(id);

        if (removedTask == null) {
            throw new IllegalArgumentException("[ЗАДАЧА НЕ НАЙДЕНА]");
        }
    }

    public void removeTasksByProjectId(String projectId) {
        taskRepository.removeAllByProjectId(projectId);
    }
}
