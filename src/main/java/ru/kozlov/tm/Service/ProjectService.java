package ru.kozlov.tm.Service;

import ru.kozlov.tm.Entity.Project;
import ru.kozlov.tm.Repository.ProjectRepository;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository){
        this.projectRepository = projectRepository;
    }

    public void removeAllProjects() {
        projectRepository.removeAll();
    }

    public Project createProject(
            String name,
            String description,
            Date dateStart,
            Date dateEnd
    ) {
        String id = UUID.randomUUID().toString();
        return projectRepository.create(id, name, description, dateStart, dateEnd);
    }

    public Map<String, Project> getAllProjects() {
        return projectRepository.getAll();
    }

    public void removeProjectById(String id) {
       Project project = projectRepository.removeById(id);

       if (project == null) {
           throw new IllegalArgumentException("[ПРОЕКТ НЕ НАЙДЕН]");
       }
    }

    public Project getProjectById(String projectId) {
        Project project = projectRepository.getById(projectId);

        if (project == null) {
            throw new IllegalArgumentException("[ПРОЕКТ НЕ НАЙДЕН]");
        }

        return project;
    }
}
